#include <stdbool.h>
#include <stdint.h>
#include "nrf.h"

#include <stdio.h>

#define RLED 13
#define BLED 14
#define GLED 15

#define DELAY 0x100000

void high_pin(uint32_t pin)
{
	NRF_GPIO->OUTSET=(1UL<<pin);
}

void low_pin(uint32_t pin)
{
	NRF_GPIO->OUTCLR=(1UL<<pin);
}

int main(void)
{
    /* Toggle LEDs. */

    //Config RGB LED
    NRF_GPIO->DIRSET= (1<<RLED);
	NRF_GPIO->PIN_CNF[RLED]=1;
    NRF_GPIO->DIRSET= (1<<BLED);
	NRF_GPIO->PIN_CNF[BLED]=1;
    NRF_GPIO->DIRSET= (1<<GLED);
	NRF_GPIO->PIN_CNF[GLED]=1;

    while (true)
    {
        
        for (uint64_t i = 0; i < DELAY; i++)
        {
            __ASM("nop");
            __ASM("nop");
            __ASM("nop");
        }

        high_pin(RLED);
        high_pin(BLED);
        high_pin(GLED);

        for (uint64_t i = 0; i < DELAY; i++)
        {
            __ASM("nop");
            __ASM("nop");
            __ASM("nop");     
        }

        low_pin(RLED);
        low_pin(BLED);
        low_pin(GLED);


    }
}